#! /usr/bin/env bash

[[ $(basename $(pwd)) == 'scripts' ]] && cd ..
marp --allow-local-files --engine ./src/engine.js --pdf -I ./ 
marp --allow-local-files --engine ./src/engine.js --pptx -I ./ 
find presentations -type f -name "*.pdf" -o -name "*.pptx" -exec mv '{}' ./dist \;
