---
marp: true
title: MARP Building Box
description: A practical introduction to possibilities offered by MARP in conjunction with various markdown-it plugins
paginate: true
---
<!-- slide heading already included in subfile -->
!include(./presentations/common/whoami.md)

---
# Footnotes
Here is a footnote reference,[^1] and another.[^longnote]

[^1]: Here is the footnote.

[^longnote]: Here's one with multiple blocks.

    Subsequent paragraphs are indented to show that they
belong to the previous footnote.

---
# Definition Lists
Term 1

:   Definition
with lazy continuation.

    Second paragraph of the definition.

Term 2 with *inline markup*

:   Definition 2

        { some code, part of Definition 2 }

    Third paragraph of definition 2.

---
# Abbreviations (And Their Definitions)
*[HTML]: Hyper Text Markup Language
*[W3C]:  World Wide Web Consortium
The HTML specification
is maintained by the W3C.

---
# Various Textual Diagram Notations

**IMPORTANT:** When including diagram definitions as files, make sure you
surround them by the markdown code block notation and include the name
of the syntax definition!

## PlantUML (Included As A File)

!include(./diagrams/example-plantuml.md)

---
## PlantUML (Inline As Markdown Code Block W/ Syntax Definition)

```plantuml
skinparam monochrome true
skinparam ranksep 20
skinparam dpi 150
skinparam arrowThickness 0.7
skinparam packageTitleAlignment left
skinparam usecaseBorderThickness 0.4
skinparam defaultFontSize 10
skinparam rectangleBorderThickness 1

rectangle "Main" {
  (main.view)
  (singleton)
}
rectangle "Base" {
  (base.component)
  (component)
  (model)
}
rectangle "<b>main.ts</b>" as main_ts

(component) ..> (base.component)
main_ts ==> (main.view)
(main.view) --> (component)
(main.view) ...> (singleton)
(singleton) ---> (model)
```

---
## Graphviz (Included As A File)

!include(./diagrams/example-graphviz.md)

---
