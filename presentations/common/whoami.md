<!-- _class: -->
# Whoami

![bg fit right:30% drop-shadow:15px,15px,15px,rgba(0,0,0,.4)](../common/profile-holger.png)

**Holger B. A. Rauch**

22 years of professional experience

*Focus on:*

- :computer: Linux/UNIX
- :eyes: EaC
- :handshake: DevOps / DevSecOps 
