- Main idea (``engine.js``) taken from (https://github.com/GeekMasher/presentations)
- Further ideas taken from the (https://kroki.io) web site (different textual diagrams);
  check whether there's a ``markdown-it`` (https://github.com/markdown-it/markdown-it)
  plugin/extension and use it

Ideas:
- Two-column layout
- Various textual diagrams (both inline and as external files)
- Footnotes

(Possible) Alternatives to MARP:
- [Pandoc](https://pandoc.org)
  - has got ext'd markdown
  - also features various possibilities for [creating slides](https://pandoc.org/MANUAL%202.html#slide-shows)
  - well-maintained [dockerfiles](https://pandoc.org/installing.html#docker) exist
- [AsciiDoctor](https://asciidoctor.org) (w/ [AsciiDoc](https://docs.asciidoctor.org/asciidoc/latest/) as format)
  - is directly supported (rendered) by Gitlab (as an alternative to GFM (Gitlab Flavored Markdown) )
  - originally written in Ruby, but Java [AsciiDoctorJ](https://docs.asciidoctor.org/asciidoctorj/latest/) and NodeJS [AsciiDoctor.js](https://docs.asciidoctor.org/asciidoctor.js/latest/) bindings exist
  - Maven ([maven-tools](https://docs.asciidoctor.org/maven-tools/latest/)) and Gradle ([asciidoctor-gradle-plugin](https://asciidoctor.org/docs/asciidoctor-gradle-plugin/)) plugins exist
  - supports many diagram types out of the box (via the [diagram-extension](https://docs.asciidoctor.org/diagram-extension/latest/))
  - also features slide creation via a [reveal.js converter](https://docs.asciidoctor.org/reveal.js-converter/latest/);
 here's a [sample/showcase](https://docs.asciidoctor.org/reveal.js-converter/latest/showcase/)
  - there's also a static site generator [antora](https://docs.antora.org/antora/latest/install-and-run-quickstart/)
  - markdown can be converted to AsciiDoc using e.g. [kramdown-asciidoc aka kramdoc](https://github.com/asciidoctor/kramdown-asciidoc)
  - and a [confluence-publisher](https://github.com/confluence-publisher/confluence-publisher) Maven plugin, which allows
    automatic publishing to Confluence - that way, you can make use of a non-linear history (by creating branches) using git/Gitlab. This can be **very** useful when documenting different features (via feature branches). Confluence only provides linear history - no branching is supported, at least not by default
- [RMarkdown](https://rmarkdown.rstudio.com/formats.html) ([books](https://bookdown.org/) are maintained on github)
  - [blogdown](https://bookdown.org/yihui/blogdown/) for (static) web site creation (alternative to [hugo](https://hugo.io) )
  - [bookdown](https://bookdown.org/yihui/bookdown/)
