const { Marp } = require('@marp-team/marp-core');

const markdownItInclude = require('markdown-it-include');
// uses katex
const markdownItPandoc = require('markdown-it-pandoc');
// uses mathjax
const markdownItPfm = require('markdown-it-pfm');
const markdownItDecorate = require('markdown-it-decorate');
const markdownItGraphvizExc = require('markdown-it-graphviz-exc');
const markdownItPlantUML = require('@markspec/markdown-it-plantuml');


const optionsInclude = {
    root: '.',
    includeRe: /!include(.+)/,
    bracesAreOptional: false
  };

module.exports = (opts) => new Marp(opts)
    .use(markdownItPlantUML)
    .use(markdownItGraphvizExc)
    .use(markdownItPandoc)
//    .use(markdownItPfm)
    .use(markdownItDecorate)
    .use(markdownItInclude, optionsInclude)
